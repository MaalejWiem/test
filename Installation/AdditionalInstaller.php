<?php

namespace Aip\SeriousgameBundle\Installation;

use Claroline\CoreBundle\Entity\Resource\MaskDecoder;
use Claroline\CoreBundle\Entity\Resource\MenuAction;
use Claroline\InstallationBundle\Additional\AdditionalInstaller as BaseInstaller;

class AdditionalInstaller extends BaseInstaller
{
    public function postInstall()
    {
        
        $this->addGameManagerRole();
    }

    public function addGameManagerRole()
    {
       
        $role = $this->container
            ->get('doctrine.orm.entity_manager')
            ->getRepository('ClarolineCoreBundle:Role')
            ->findOneByName('ROLE_GAME_MANAGER');

        if (!$role) {
            $this->container
                ->get('claroline.manager.role_manager')
                ->createBaseRole('ROLE_GAME_MANAGER', 'game_manager');
        }
    }
}
