<?php

namespace Aip\SeriousgameBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConfigurationgameEditType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder->add('nom', 'text', array('required' => true , 'read_only' =>  true));
    	$builder->add('version', 'choice', array(
    			'choices'   => array(
    					'3'   => '3',
    					'4' => '4',
    					'5'   => '5',
    			),
    			'multiple'  => false,
    			
    	));
    	$builder->add('menu', 'textarea', array('attr' => array(
    			'required' => true,
    			'rows' => '7','cols' => '10')));
    	
    	$builder->add('url', 'url', array('required' => true));
    	$builder->add('urltrace', 'url', array('required' => true));
    	$builder->add('urlUM', 'url', array('required' => true));
    	$builder->add('scenario', 'textarea', array('attr' => array(
    			'required' => true,
    			'rows' => '7','cols' => '10')));
    	$builder->add('envgraphique', 'textarea', array('attr' => array(
    			'required' => true,
    			'rows' => '7','cols' => '10')));
    	
    	
    	
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aip\SeriousgameBundle\Entity\Configurationgame',
        	'translation_domain' => 'game'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'game_form';
    }
}
