<?php

/*
 * This file is part of the Claroline Connect package.
 *
 * (c) Claroline Consortium <consortium@claroline.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Aip\SeriousgameBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActivitegameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', 'text', array('required' => false));
       
         
        $builder->add('content', 'tinymce', array('required' => true));
        
        $activitegame=$options['data'];
        $aggregate=$activitegame->getAggregate();
        $builder->add('nomconfiguration','entity',array(
        		'class'=>'AipSeriousgameBundle:Configurationgame',
        		'property' => 'Nom',
        		'query_builder' => function(\Doctrine\ORM\EntityRepository $er) use ($aggregate)
        		{
        
        			return $er->createQueryBuilder('r')->where('r.aggregate = :id')->setParameter('id',$aggregate->getId());
        		},
        		'mapped' => false,
        		 
        		 
        ));
        
    }

    public function getName()
    {
        return 'activitegame_form';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'translation_domain' => 'game'
            )
        );
    }
}
