<?php

namespace Aip\SeriousgameBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Claroline\CoreBundle\Entity\Resource\AbstractResource;

/**
 * @ORM\Entity
 * @ORM\Table(name="aip_trace_jeux")
 */
class TraceLaAggreagate extends AbstractResource
{
   

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected  $id;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idTrace", type="string", length=255,nullable=true)
	 */
	private $idTrace;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="endz", type="string", length=255,nullable=true)
	 */
	private $endz;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="server", type="string", length=255,nullable=true)
	 */
	private $server;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="endx", type="string", length=255,nullable=true)
	 */
	private $endx;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="startz", type="string", length=255,nullable=true)
	 */
	private $startz;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="latime", type="string", length=255,nullable=true)
	 */
	private $latime;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="startx", type="string", length=255,nullable=true)
	 */
	private $startx;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="time", type="string", length=255,nullable=true)
	 */
	private $time;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="login", type="string", length=255,nullable=true)
	 */
	private $login;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=255,nullable=true)
	 */
	private $type;
	
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set idTrace
	 *
	 * @param string $idTrace
	 * @return TraceApprenants
	 */
	public function setIdTrace($idTrace)
	{
		$this->idTrace = $idTrace;
	
		return $this;
	}
	
	/**
	 * Get idTrace
	 *
	 * @return string
	 */
	public function getIdTrace()
	{
		return $this->idTrace;
	}
	
	/**
	 * Set endz
	 *
	 * @param string $endz
	 * @return TraceApprenants
	 */
	public function setEndz($endz)
	{
		$this->endz = $endz;
	
		return $this;
	}
	
	/**
	 * Get endz
	 *
	 * @return string
	 */
	public function getEndz()
	{
		return $this->endz;
	}
	
	/**
	 * Set server
	 *
	 * @param string $server
	 * @return TraceApprenants
	 */
	public function setServer($server)
	{
		$this->server = $server;
	
		return $this;
	}
	
	/**
	 * Get server
	 *
	 * @return string
	 */
	public function getServer()
	{
		return $this->server;
	}
	
	/**
	 * Set endx
	 *
	 * @param string $endx
	 * @return TraceApprenants
	 */
	public function setEndx($endx)
	{
		$this->endx = $endx;
	
		return $this;
	}
	
	/**
	 * Get endx
	 *
	 * @return string
	 */
	public function getEndx()
	{
		return $this->endx;
	}
	
	/**
	 * Set startz
	 *
	 * @param string $startz
	 * @return TraceApprenants
	 */
	public function setStartz($startz)
	{
		$this->startz = $startz;
	
		return $this;
	}
	
	/**
	 * Get startz
	 *
	 * @return string
	 */
	public function getStartz()
	{
		return $this->startz;
	}
	
	/**
	 * Set latime
	 *
	 * @param string $latime
	 * @return TraceApprenants
	 */
	public function setLatime($latime)
	{
		$this->latime = $latime;
	
		return $this;
	}
	
	/**
	 * Get latime
	 *
	 * @return string
	 */
	public function getLatime()
	{
		return $this->latime;
	}
	
	/**
	 * Set startx
	 *
	 * @param string $startx
	 * @return TraceApprenants
	 */
	public function setStartx($startx)
	{
		$this->startx = $startx;
	
		return $this;
	}
	
	/**
	 * Get startx
	 *
	 * @return string
	 */
	public function getStartx()
	{
		return $this->startx;
	}
	
	/**
	 * Set time
	 *
	 * @param string $time
	 * @return TraceApprenants
	 */
	public function setTime($time)
	{
		$this->time = $time;
	
		return $this;
	}
	
	/**
	 * Get time
	 *
	 * @return string
	 */
	public function getTime()
	{
		return $this->time;
	}
	
	/**
	 * Set login
	 *
	 * @param string $login
	 * @return TraceApprenants
	 */
	public function setLogin($login)
	{
		$this->login = $login;
	
		return $this;
	}
	
	/**
	 * Get login
	 *
	 * @return string
	 */
	public function getLogin()
	{
		return $this->login;
	}
	
	/**
	 * Set type
	 *
	 * @param string $type
	 * @return TraceApprenants
	 */
	public function setType($type)
	{
		$this->type = $type;
	
		return $this;
	}
	
	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}
	
	
	
	protected $configurationgame;
	
	public function getConfigurationgame()
	{
		return $this->configurationgame;
	}
	
}