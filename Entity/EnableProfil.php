<?php

namespace Aip\SeriousgameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnableProfil
 *
 * @ORM\Table(name="aip_jeuxProfils")
 * @ORM\Entity
 */
class EnableProfil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="enableprofil", type="boolean", length=255)
     */
    private $enableprofil;
    /**
     *
     *
     * @ORM\Column(name="aggregate_id", type="integer", length=255)
     */
    protected $aggregate;
    
    /**
     *
     *
     * @ORM\Column(name="userid", type="integer",nullable= true)
     */
    private $userid;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    public function setEnableprofil($enableprofil)
    {
        $this->enableprofil = $enableprofil;

        return $this;
    }

public function isEnableprofil()
    {
    	return $this->enableprofil;
    }
    public function getAggregate()
    {
    	return $this->aggregate;
    }
    
    public function setAggregate($aggregate)
    {
    	$this->aggregate= $aggregate;
    }
    
    public function getUserid()
    {
    	return $this->userid;
    }
    
    public function setUserid($userid)
    {
    	$this->userid= $userid;
    }
}
