<?php

namespace Aip\SeriousgameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Claroline\CoreBundle\Entity\Resource\AbstractResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Configurationgame
 *
 * @ORM\Table(name="aip_configuration_jeux")
 * @ORM\Entity(repositoryClass="Aip\SeriousgameBundle\Repository\ConfigurationgameRepository")
 */
class Configurationgame extends AbstractResource implements JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string",nullable=true)
     * @Assert\NotBlank()
     */
    protected $url;
    /**
     * @var string
     *
     * @ORM\Column(name="envgraphique", type="text", nullable=true)
     */
    protected $envgraphique;
    /**
     * @var string
     *
     * @ORM\Column(name="menu", type="text",nullable=true)
     * @Assert\NotBlank()
     */
   protected $menu;
    /**
     * @var string
     *
     * @ORM\Column(name="urltrace", type="string", nullable=true)
     */
    protected $urltrace;
    /**
     * @var string
     *
     * @ORM\Column(name="urlUM", type="string", nullable=true)
     */
   protected $urlUM;
    /**
     * @var  string
     *
     * @ORM\Column(name="version", type="string", nullable=true)
     */
   protected $version;
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string",nullable=true)
     * @Assert\NotBlank()
     */
    protected $nom;
    /**
     * @var string
     *
     * @ORM\Column(name="urlserveur", type="string", nullable=true)
     */
   protected $urlserveur;
    /**
     * @var string
     *
     * @ORM\Column(name="urlfichier", type="string", nullable=true)
     */
   protected $urlfichier;

    

    /**
     * Set urlserveur
     *
     * @param string $urlserveur
     * @return Configurationgame
     */
    public function setUrlserveur($urlserveur)
    {
    	$this->urlserveur = $urlserveur;
    
    	return $this;
    }
    /**
     * Get urlserveur
     *
     * @return string
     */
    public function getUrlserveur()
    {
    	return $this->urlserveur;
    }
    /**
     * Set urlfichier
     *
     * @param string $urlfichier
     * @return Configurationgame
     */
    public function setUrlfichier($urlfichier)
    {
    	$this->urlfichier = $urlfichier;
    
    	return $this;
    }
    /**
     * Get urlfichier
     *
     * @return string
     */
    public function getUrlfichier()
    {
    	return $this->urlfichier;
    }

    /**
     *
     *
     * @ORM\Column(name="scenario", type="text")
     * @Assert\NotBlank()
     */
    private $scenario;
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Claroline\CoreBundle\Entity\User"
     * )
     * @ORM\JoinColumn(name="creator_id", onDelete="CASCADE", nullable=false)
     */
    protected $creator;
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Aip\SeriousgameBundle\Entity\ConfigurationgameAggregate",
     *     inversedBy="configurationgame"
     * )
     * @ORM\JoinColumn(name="aggregate_id", onDelete="CASCADE", nullable=false)
     */
    protected $aggregate;
    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    protected $creationDate;
    /**
     * @ORM\Column(name="publication_date", type="datetime", nullable=true)
     */
    protected $publicationDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Configurationgame
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * Set version
     *
     * @param integer $version
     * @return Configurationgame
     */
    public function setVersion($version)
    {
    	$this->version = $version;
    	return $this;
    }
    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
    	return $this->version;
    }
    /**
     * Set menu
     *
     * @param text $menu
     * @return Configurationgame
     */
    public function setMenu($menu)
    {
    	$this->menu = $menu;
    
    	return $this;
    }
    /**
     * Get menu
     *
     * @return text
     */
    public function getMenu()
    {
    	return $this->menu;
    }
    /**
     * Set urltrace
     *
     * @param string $urltrace
     * @return Configurationgame
     */
    public function setUrltrace($urltrace)
    {
    	$this->urltrace = $urltrace;
    
    	return $this;
    }
    /**
     * Get urltrace
     *
     * @return string
     */
    public function getUrltrace()
    {
    	return $this->urltrace;
    }
    /**
     * Set urlUM
     *
     * @param string $urlUM
     * @return Configurationgame
     */
    public function setUrlUM($urlUM)
    {
    	$this->urlUM = $urlUM;
    
    	return $this;
    }
    /**
     * Get urlUM
     *
     * @return string
     */
    public function getUrlUM()
    {
    	return $this->urlUM;
    }
    /**
     * Set envgraphique
     *
     * @param text $envgraphique
     * @return Configurationgame
     */
    public function setEnvgraphique($envgraphique)
    {
    	$this->envgraphique = $envgraphique;
    
    	return $this;
    }
    /**
     * Get envgraphique
     *
     * @return text
     */
    public function getEnvgraphique()
    {
    	return $this->envgraphique;
    }
   
    /**
     * Set scenario
     *
     * @param text $scenario
     * @return Configurationgame
     */
    public function setScenario($scenario)
    {
        $this->scenario = $scenario;

        return $this;
    }

    /**
     * Get scenario
     *
     * @return text 
     */
    public function getScenario()
    {
        return $this->scenario;
    }
    public function getCreator()
    {
    	return $this->creator;
    }
    
    public function setNom($nom)
    {
    	$this->nom = $nom;
    }
    public function getNom()
    {
    	return $this->nom;
    }
    
    public function setCreator($creator)
    {
    	$this->creator = $creator;
    }
    public function getAggregate()
    {
    	return $this->aggregate;
    }
    
    public function setAggregate($aggregate)
    {
    	$this->aggregate= $aggregate;
    }
    public function getCreationDate()
    {
    	return $this->creationDate;
    }
    
    public function setCreationDate($creationDate)
    {
    	$this->creationDate = $creationDate;
    }
    public function getPublicationDate()
    {
    	return $this->publicationDate;
    }
    
    public function setPublicationDate($publicationDate)
    {
    	$this->publicationDate = $publicationDate;
    }
    public function jsonSerialize()
    {
    	return array(
    			'nom'=> $this->nom,
    			'version'=> $this->version,
    			'menu'=> $this->menu,
    			'url' => $this->url,
    			'urltrace'=> $this->urltrace,
    			'urlUM'=> $this->urlUM,
    			'scenario'=> $this->scenario,
    			'envgraphique'=> $this->envgraphique,
    					
    	);
    }
}
