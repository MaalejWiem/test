<?php

/*
 * This file is part of the Claroline Connect package.
 *
 * (c) Claroline Consortium <consortium@claroline.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Aip\SeriousgameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Aip\SeriousgameBundle\Repository\ActivitegameRepository")
 * @ORM\Table(name="aip_activite_jeux")
 */
class Activitegame
{
   /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
    /**
     * @var string
     *
     * @ORM\Column(name="nomconfiguration", type="string", length=255)
     */
    private $nomconfiguration;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    protected $content;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="Claroline\CoreBundle\Entity\User" 
     * )
     * @ORM\JoinColumn(name="creator_id", onDelete="CASCADE", nullable=false)
     */
    protected $creator;
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Aip\SeriousgameBundle\Entity\ConfigurationgameAggregate",
     *     inversedBy="activitegame"
     * )
     * @ORM\JoinColumn(name="aggregate_id", onDelete="CASCADE", nullable=false)
     */
    protected $aggregate;
    
    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    protected $creationDate;
    /**
     * @ORM\Column(name="publication_date", type="datetime", nullable=true)
     */
    protected $publicationDate;
    

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setNom($nom)
    {
    	$this->nom = $nom;
    
    	return $this;
    }
    
    
    public function getNom()
    {
    	return $this->nom;
    }
    
    public function getNomconfiguration()
    {
    	return $this->nomconfiguration;
    }
    
    public function setNomconfiguration($nomconfiguration)
    {
    	$this->nomconfiguration = $nomconfiguration;
    }
    
    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getAnnouncer()
    {
        return $this->announcer;
    }

    public function setAnnouncer($announcer)
    {
        $this->announcer = $announcer;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    public function setPublicationDate($publicationDate)
    {
        $this->publicationDate = $publicationDate;
    }

    public function isVisible()
    {
        return $this->visible;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    public function getVisibleFrom()
    {
        return $this->visibleFrom;
    }

    public function setVisibleFrom($visibleFrom)
    {
        $this->visibleFrom = $visibleFrom;
    }

    public function getVisibleUntil()
    {
        return $this->visibleUntil;
    }

    public function setVisibleUntil($visibleUntil)
    {
        $this->visibleUntil = $visibleUntil;
    }

    public function getCreator()
    {
        return $this->creator;
    }

    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    public function getAggregate()
    {
        return $this->aggregate;
    }

    public function setAggregate($aggregate)
    {
        $this->aggregate = $aggregate;
    }
}