<?php

namespace Aip\SeriousgameBundle\Migrations\pdo_mysql;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated migration based on mapping information: modify it with caution
 *
 * Generation date: 2016/02/18 05:14:38
 */
class Version20160218171434 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("
            CREATE TABLE aip_jeuxProfils (
                id INT AUTO_INCREMENT NOT NULL, 
                enableprofil TINYINT(1) NOT NULL, 
                aggregate_id INT NOT NULL, 
                userid INT DEFAULT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_jeux_Lancement (
                id INT AUTO_INCREMENT NOT NULL, 
                enable TINYINT(1) NOT NULL, 
                creator INT NOT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_sac_jeux (
                id INT AUTO_INCREMENT NOT NULL, 
                resourceNode_id INT DEFAULT NULL, 
                UNIQUE INDEX UNIQ_9535DB57B87FAB32 (resourceNode_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_activite_jeux (
                id INT AUTO_INCREMENT NOT NULL, 
                creator_id INT NOT NULL, 
                aggregate_id INT NOT NULL, 
                nom VARCHAR(255) NOT NULL, 
                nomconfiguration VARCHAR(255) NOT NULL, 
                content LONGTEXT NOT NULL, 
                creation_date DATETIME NOT NULL, 
                publication_date DATETIME DEFAULT NULL, 
                INDEX IDX_E1A4A19361220EA6 (creator_id), 
                INDEX IDX_E1A4A193D0BBCCBE (aggregate_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_jeux (
                id INT AUTO_INCREMENT NOT NULL, 
                resourceNode_id INT DEFAULT NULL, 
                UNIQUE INDEX UNIQ_C03D49B0B87FAB32 (resourceNode_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_configuration_jeux (
                id INT AUTO_INCREMENT NOT NULL, 
                creator_id INT NOT NULL, 
                aggregate_id INT NOT NULL, 
                url VARCHAR(255) DEFAULT NULL, 
                envgraphique LONGTEXT DEFAULT NULL, 
                menu LONGTEXT DEFAULT NULL, 
                urltrace VARCHAR(255) DEFAULT NULL, 
                urlUM VARCHAR(255) DEFAULT NULL, 
                version VARCHAR(255) DEFAULT NULL, 
                nom VARCHAR(255) DEFAULT NULL, 
                urlserveur VARCHAR(255) DEFAULT NULL, 
                urlfichier VARCHAR(255) DEFAULT NULL, 
                scenario LONGTEXT NOT NULL, 
                creation_date DATETIME NOT NULL, 
                publication_date DATETIME DEFAULT NULL, 
                resourceNode_id INT DEFAULT NULL, 
                INDEX IDX_C3DB907661220EA6 (creator_id), 
                INDEX IDX_C3DB9076D0BBCCBE (aggregate_id), 
                UNIQUE INDEX UNIQ_C3DB9076B87FAB32 (resourceNode_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_jeuxLA (
                id INT AUTO_INCREMENT NOT NULL, 
                enablela TINYINT(1) NOT NULL, 
                userid INT DEFAULT NULL, 
                aggregate_id INT NOT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_jeuxCLA (
                id INT AUTO_INCREMENT NOT NULL, 
                enablecla TINYINT(1) NOT NULL, 
                userid INT DEFAULT NULL, 
                aggregate_id INT NOT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            CREATE TABLE aip_trace_jeux (
                id INT AUTO_INCREMENT NOT NULL, 
                idTrace VARCHAR(255) DEFAULT NULL, 
                endz VARCHAR(255) DEFAULT NULL, 
                server VARCHAR(255) DEFAULT NULL, 
                endx VARCHAR(255) DEFAULT NULL, 
                startz VARCHAR(255) DEFAULT NULL, 
                latime VARCHAR(255) DEFAULT NULL, 
                startx VARCHAR(255) DEFAULT NULL, 
                time VARCHAR(255) DEFAULT NULL, 
                login VARCHAR(255) DEFAULT NULL, 
                type VARCHAR(255) DEFAULT NULL, 
                resourceNode_id INT DEFAULT NULL, 
                UNIQUE INDEX UNIQ_F580FB36B87FAB32 (resourceNode_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ");
        $this->addSql("
            ALTER TABLE aip_sac_jeux 
            ADD CONSTRAINT FK_9535DB57B87FAB32 FOREIGN KEY (resourceNode_id) 
            REFERENCES claro_resource_node (id) 
            ON DELETE CASCADE
        ");
        $this->addSql("
            ALTER TABLE aip_activite_jeux 
            ADD CONSTRAINT FK_E1A4A19361220EA6 FOREIGN KEY (creator_id) 
            REFERENCES claro_user (id) 
            ON DELETE CASCADE
        ");
        $this->addSql("
            ALTER TABLE aip_activite_jeux 
            ADD CONSTRAINT FK_E1A4A193D0BBCCBE FOREIGN KEY (aggregate_id) 
            REFERENCES aip_jeux (id) 
            ON DELETE CASCADE
        ");
        $this->addSql("
            ALTER TABLE aip_jeux 
            ADD CONSTRAINT FK_C03D49B0B87FAB32 FOREIGN KEY (resourceNode_id) 
            REFERENCES claro_resource_node (id) 
            ON DELETE CASCADE
        ");
        $this->addSql("
            ALTER TABLE aip_configuration_jeux 
            ADD CONSTRAINT FK_C3DB907661220EA6 FOREIGN KEY (creator_id) 
            REFERENCES claro_user (id) 
            ON DELETE CASCADE
        ");
        $this->addSql("
            ALTER TABLE aip_configuration_jeux 
            ADD CONSTRAINT FK_C3DB9076D0BBCCBE FOREIGN KEY (aggregate_id) 
            REFERENCES aip_jeux (id) 
            ON DELETE CASCADE
        ");
        $this->addSql("
            ALTER TABLE aip_configuration_jeux 
            ADD CONSTRAINT FK_C3DB9076B87FAB32 FOREIGN KEY (resourceNode_id) 
            REFERENCES claro_resource_node (id) 
            ON DELETE CASCADE
        ");
        $this->addSql("
            ALTER TABLE aip_trace_jeux 
            ADD CONSTRAINT FK_F580FB36B87FAB32 FOREIGN KEY (resourceNode_id) 
            REFERENCES claro_resource_node (id) 
            ON DELETE CASCADE
        ");
    }

    public function down(Schema $schema)
    {
        $this->addSql("
            ALTER TABLE aip_activite_jeux 
            DROP FOREIGN KEY FK_E1A4A193D0BBCCBE
        ");
        $this->addSql("
            ALTER TABLE aip_configuration_jeux 
            DROP FOREIGN KEY FK_C3DB9076D0BBCCBE
        ");
        $this->addSql("
            DROP TABLE aip_jeuxProfils
        ");
        $this->addSql("
            DROP TABLE aip_jeux_Lancement
        ");
        $this->addSql("
            DROP TABLE aip_sac_jeux
        ");
        $this->addSql("
            DROP TABLE aip_activite_jeux
        ");
        $this->addSql("
            DROP TABLE aip_jeux
        ");
        $this->addSql("
            DROP TABLE aip_configuration_jeux
        ");
        $this->addSql("
            DROP TABLE aip_jeuxLA
        ");
        $this->addSql("
            DROP TABLE aip_jeuxCLA
        ");
        $this->addSql("
            DROP TABLE aip_trace_jeux
        ");
    }
}