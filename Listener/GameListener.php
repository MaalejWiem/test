<?php

namespace Aip\SeriousgameBundle\Listener;

use Aip\SeriousgameBundle\Entity\ConfigurationgameAggregate;
use Aip\SeriousgameBundle\Entity\Configurationgame;
use Aip\SeriousgameBundle\Form\ConfigurationgameType;
//use Claroline\CoreBundle\Event\CopyResourceEvent;
use Claroline\CoreBundle\Event\CreateFormResourceEvent;
use Claroline\CoreBundle\Event\CreateResourceEvent;
use Claroline\CoreBundle\Event\DeleteResourceEvent;
//use Claroline\CoreBundle\Event\DownloadResourceEvent;
use Claroline\CoreBundle\Event\OpenResourceEvent;
use Claroline\CoreBundle\Form\Factory\FormFactory;
use Claroline\CoreBundle\Manager\ResourceManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\SecurityContextInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service()
 */
class GameListener
{
    private $formFactory;
    private $request;
    private $resourceManager;
    private $router;
    private $templating;
    private $container;
    protected $securityContext;

    /**
     * @DI\InjectParams({
     *     "formFactory"        = @DI\Inject("claroline.form.factory"),
     *     "requestStack"       = @DI\Inject("request_stack"),
     *     "resourceManager"    = @DI\Inject("claroline.manager.resource_manager"),
     *     "router"             = @DI\Inject("router"),
     *     "securityContext"    = @DI\Inject("security.context"),
     *     "templating"         = @DI\Inject("templating")
    
     * })
     */
    public function __construct(
        FormFactory $formFactory,
        RequestStack $requestStack,
        ResourceManager $resourceManager,
        TwigEngine $templating,
        UrlGeneratorInterface $router,
    	SecurityContextInterface $securityContext
    )
    {
        $this->formFactory = $formFactory;
        $this->request = $requestStack->getCurrentRequest();
        $this->resourceManager = $resourceManager;
        $this->router = $router;
        $this->templating = $templating;
        $this->securityContext = $securityContext;
    
    }

    /**
     * @DI\Observe("create_form_Jeux sérieux")
     *
     * @param CreateFormResourceEvent $event
     */
    public function onCreateForm(CreateFormResourceEvent $event)
    {
    	$user = $this->securityContext->getToken()->getUser();
    	$roles = $user->getRoles();
    	$role_enseignant = false;
    	foreach ($roles  as $V)
    	{
    		if ($V == "ROLE_GAME_MANAGER")
    		{
    			 
    			$role_enseignant= true;
    		}
    		 
    	}
    	if ($role_enseignant == true)
    	{
    	$form = $this->formFactory->create(
            FormFactory::TYPE_RESOURCE_RENAME,
            array(),
            new ConfigurationgameAggregate()
        );
        $content = $this->templating->render(
            'ClarolineCoreBundle:Resource:createForm.html.twig',
            array(
                'form' => $form->createView(),
                'resourceType' => 'Jeux sérieux'
            )
        );
        $event->setResponseContent($content);
        $event->stopPropagation();
        }
        else
        {
        	$content = $this->templating->render(
        			'AipSeriousgameBundle::echecForm.html.twig',
        			array('user' => $user,
        					'roles' => $roles ));
        	$event->setResponseContent($content);
        	$event->stopPropagation();
        }
    }

    /**
     * @DI\Observe("create_Jeux sérieux")
     *
     * @param CreateResourceEvent $event
     */
    public function onCreate(CreateResourceEvent $event)
    {
    	if (!$this->request) {
    		throw new NoHttpRequestException();
    	}
    	$form = $this->formFactory->create(
    			FormFactory::TYPE_RESOURCE_RENAME,
    			array(),
    			new ConfigurationgameAggregate()
    	);
    	$form->handleRequest($this->request);
    
    	if ($form->isValid()) {
    		$configurationgameAggregate = $form->getData();
    		$event->setResources(array($configurationgameAggregate));
    		$event->stopPropagation();
    
    		return;
    	}
    
    	$content = $this->templating->render(
    			'ClarolineCoreBundle:Resource:createForm.html.twig',
    			array(
    					'form' => $form->createView(),
    					'resourceType' => 'Jeux sérieux'
    			)
    	);
    	$event->setErrorFormContent($content);
    	$event->stopPropagation();
    }
    /**
     * @DI\Observe("delete_Jeux sérieux")
     *
     * @param DeleteResourceEvent $event
     */
    public function onDelete(DeleteResourceEvent $event)
    {
    	//        $this->resourceManager->delete($event->getResource());
    	$event->stopPropagation();
    }
    /**
     * @DI\Observe("open_Jeux sérieux")
     *
     * @param OpenResourceEvent $event
     */
    public function onOpen(OpenResourceEvent $event)
    {
    	$route = $this->router->generate(
    			'claro_activitegame_list',
    			array('aggregateId' => $event->getResource()->getId())
    	);
    	$event->setResponse(new RedirectResponse($route));
    	$event->stopPropagation();
    }
    
}